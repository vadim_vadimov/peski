<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Золотые Пески - Коттеджный посёлок в Подмосковье</title>

	<meta name="description" content="Золотые Пески в Подмосковье - Коттеджный посёлок бизнес-класса на берегу Можайскоо водохранилища. Дома с участок и коммуникациями у воды.">

	<meta name="keywords" content="Золотые Пески, Москва, Можайск, Можайское водохранилище, Дом, Дача, Участок">
	<link rel="stylesheet"  href="css/style.css">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '297698297598052');
        fbq('track', 'PageView_zp');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=297698297598052&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>

	<div class="main-wrap">
		<div class="intro-block" style="background-image: url(img/intro-bg.jpg);">
			<div class="intro-block__video">
				<video autoplay muted loop id="video-bg">
				   <source src="video-bg.mp4" type="video/mp4">
				</video>
			</div>

			<header class="header">
				<div class="container">
					<div class="header__logo-content">
						<div class="header__logo-wrap">
							<a href="/" class="header__logo"><img src="img/logo.svg" alt="logo"></a>
							<span>Коттеджный поселок бизнес-класса</span>
						</div>
						<div class="header__list">
							<a href="#map" class="header__info-item">
								<img src="img/h-item-1.png" alt="item">
								<div class="header__item-text">
									<p>15 мин. от г. Можайск</p>
									<p>100 км от МКАД</p>
								</div>
							</a>
							<div class="header__call-item">
								<img src="img/h-item-2.png" alt="item">
								<div class="header__item-text">
									<a data-fancybox data-src="#popup-1" href="javascript://">записаться на просмотр</a>
								</div>
							</div>
						</div>
					</div>
					<div class="header__tel-content">
						<a href="tel: 89999992511" class="header__tel jsTrackContact">8 999 999 25 11</a>
						<a data-fancybox data-src="#popup-1" href="javascript://" class="header__call-btn">Позвоните мне</a>
					</div>
					<a href="tel:89999992511" class="header__tel--mobile jsTrackContact"><img src="img/tel.svg" alt="tel"></a>
				</div>
			</header>

			<div class="intro">
				<div class="container">
					<div class="intro__content">
						<div class="intro__title">
							<h1>Дома от 135 м² c участками<br><span class="intro__italic">в закрытом поселке</span> на берегу Можайского моря
								<span class="intro__title-description">От <span class="intro__decor">3,5 млн. руб.</span></span>
							</h1>
						</div>
						<p class="intro__text">Поселок заселен. Все коммуникации уже проведены</p>
						<a data-fancybox data-src="#popup-2" href="javascript://" class="btn-main"><span>Подобрать дом под свой бюджет</span></a>
					</div>
					<div class="intro__btn-wrap">
						<a data-fancybox href="#myVideo" class="intro__btn"><img src="img/play.svg" alt="play"></a>
						<p>Смотреть видео о коттеджном поселке</p>
						<div class="circle-pulse  circle-1"></div>
						<div class="circle-pulse  circle-2"></div>
						<div class="circle-pulse  circle-3"></div>
						<div class="circle-pulse  circle-4"></div>
					</div>
				</div>
			</div>

			<div class="parallax-wrap">
				<div class="parallax-item  layer" data-depth="0.05">
					<img src="img/sky-p-1.png" alt="sky-parallax">
				</div>
				<div class="parallax-item  layer" data-depth="0.12">
					<img src="img/sky-p-2.png" alt="sky-parallax">
				</div>
			</div>
		</div>

		<div class="content-wrap">
			<section class="presentation">
				<div class="container">
					<div class="main-title">
						<h2><span>Скачайте полную презентацию</span> коттеджного поселка бизнес-класса «Золотые пески»</h2>
					</div>
					<div class="main-form">
						<div class="main-form__wrap">
							<div class="main-form__title-content">
								<h4 class="main-form__title">Введите<br><span>ваши данные,</span></h4>
								<p class="main-form__subtitle">чтобы скачать презентацию в 1 клик</p>
							</div>
							<form action="" class="name-tel-form" novalidate="novalidate">
								<div class="main-form__field">
									<input type="text" name="name" placeholder="Введите ваше имя">
								</div>
								<div class="main-form__field">
									<input type="tel" name="tel" placeholder="Введите ваш телефон">
								</div>
								<span class="main-form__small-text">Презентация откроется автоматически в новой вкладке</span>
								<button class="btn-main"><span>Скачать презентацию</span></button>
								<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
								<input type="hidden" name="form_name" value="Скачать полную презентацию">
                                <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
							</form>
						</div>
					</div>
					<img src="img/book.png" alt="book" class="presentation__book">
					<div class="parallax-wrap  parallax-wrap--index">
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/item-p-1.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.4">
							<img src="img/berry-p-2.png" alt="parallax">
						</div>
					</div>
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.1">
							<img src="img/leaf-p-1.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.3">
							<img src="img/leaf-p-2.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.5">
							<img src="img/berry-p-1.png" alt="parallax">
						</div>
					</div>
				</div>
			</section>

			<section class="facilities" style="background-image: url(img/facilities-bg.jpg);">
				<div class="container">
					<div class="facilities__title-content">
						<div class="main-title">
							<h2>Вы будете иметь <span>доступ ко всем удобствам</span> современного курорта</h2>
							<p>Всего в 1,5 часах езды от Москвы</p>
						</div>
						<div class="facilities__num-item">
							<p>36 <span>Га</span></p>
							<span class="facilities__num-text">общая площадь рекреационной зоны</span>
						</div>
					</div>
					<div class="facilities__list">
						<div class="facilities__item">
							<div class="facilities__image">
								<img src="img/facilities-1.png" alt="facilities">
							</div>	
							<strong>База отдыха</strong>
							<p>Зоны барбекью, комплекс бань, вейк-станция для водных видов спорта</p>
						</div>
						<div class="facilities__item">
							<div class="facilities__image">
								<img src="img/facilities-2.png" alt="facilities">
							</div>	
							<strong>Причалы</strong>
							<p>3 причала для лодок, катеров, гидроциклов и катамаранов</p>
						</div>
						<div class="facilities__item">
							<div class="facilities__image">
								<img src="img/facilities-3.png" alt="facilities">
							</div>	
							<strong>Спортплощадки</strong>
							<p>Для игры в волейбол<br>и баскетбол</p>
						</div>
						<div class="facilities__item">
							<div class="facilities__image">
								<img src="img/facilities-4.png" alt="facilities">
							</div>	
							<strong>Пляж</strong>
							<p>Благоустроенный<br>песчаный пляж с<br>мелким желтым песком</p>
						</div>
						<div class="facilities__item">
							<div class="facilities__image">
								<img src="img/facilities-5.png" alt="facilities">
							</div>	
							<strong>Экологическая тропа</strong>
							<p>По территории проходят тропы диких лосей и кабанов</p>
						</div>
					</div>
					<div class="facilities__description">
						<div class="facilities__description-wrap">
							<p>Вы сможете восстанавливать силы и полноценно отдыхать в своем загородном доме</p>
						</div>
					</div>
				</div>
			</section>

			<section class="region">
				<div class="container">
					<div class="main-title">
						<h2>На первой береговой линии<br><span>в 2 минутах от соснового леса</span></h2>
					</div>
					<div class="region__list">
						<div class="region__item">
							<div class="region__image">
								<img src="img/region-1.png" alt="region">
							</div>
							<strong>Рыбалка</strong>
							<p>В Можайском водохранилище водятся уникальные для Подмосковья виды рыб: стерлядь, голавль, густера и др.</p>
						</div>
						<div class="region__item">
							<div class="region__image">
								<img src="img/region-2.png" alt="region">
							</div>
							<strong>Грибы и ягоды</strong>
							<p>Хвойные леса богаты грибами и ягодами, которые вы сможете собирать с июня по октябрь</p>
						</div>
						<div class="region__item">
							<div class="region__image">
								<img src="img/region-3.png" alt="region">
							</div>
							<strong>Охота</strong>
							<p>В охотничьем хозяйстве<br>можно организовать охоту<br>на кабана, лося, косулю,<br>мелкую дичь и птицу</p>
						</div>
						<div class="region__item">
							<div class="region__image">
								<img src="img/region-4.png" alt="region">
							</div>
							<strong>Снегоход<br>и зимние виды спорта</strong>
							<p>По льду Можайского водохранилища вы сможете кататься на снегоходе. Рядом с поселком оборудованы лыжные трассы</p>
						</div>
					</div>
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.1">
							<img src="img/leaf-p-1.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.3">
							<img src="img/leaf-p-3.png" alt="parallax">
						</div>
					</div>
				</div>
			</section>

			<section class="district">
				<div class="container">
					<div class="district__content">
						<div class="main-title">
							<h2>Район, <span>пожалуй,<br>с лучшей экологией</span> во всем Подмосковье</h2>
							<p>От автотрассы поселок отделяет лесополоса шириной более километра</p>
						</div>
						<ul class="list-big">
							<li>Поселок расположен на полуострове<br>в окружении сосновых лесов</li>
							<li>Здесь нет промышленных<br>производств, загруженных автотрасс</li>
							<li>Вода, воздух и почва признаны одними<br>из самых чистых в регионе</li>
						</ul>
					</div>
					<div class="district__info">
						<img src="img/leaf.png" alt="leaf">
						<a href="https://nedvio.com/ekologia-rayonov-podmoskovya/" target="_blank" class="district__link">Узнайте больше<br>об экологии Можайского района</a>
					</div>
				</div>
			</section>

			<section class="selection">
				<div class="container">
					<div class="main-title">
						<h2><span>Подберите за 30 секунд</span> дом на<br>берегу Можайского водохранилища под ваш бюджет</h2>
						<p>Ответьте на 3 вопроса и получите до 5 вариантов домов</p>
					</div>
					<div class="selection__main-container">
						<div class="selection__main">
							<div class="selection__wrap">
								<form action="#" class="selection-form">
									<div class="selection__inner">
										<div class="selection__questions step-1">
											<div class="selection__question-bar"></div>
											<span>Вопрос <span class="jsSelectionStep">1</span> из 3</span>
										</div>

										<div class="selection__item-title-wrap">
											<strong class="selection__item-title step-1">1. Укажите ваш бюджет</strong>
											<strong class="selection__item-title step-2">2. Укажите площадь дома, от</strong>
											<strong class="selection__item-title step-3">3. Укажите площадь участка</strong>
										</div>

										<div class="selection__step step-1">
											<div class="selection__slider">
												<div class="selection__select-price-item">
													<span class="selection__select-price" id="range-1-span">1</span>
													<span class="rub">P</span>
												</div>
												<div id="range-1"></div>
												<input type="hidden" name="range_1">
											</div>
											<div class="selection__price-container">
												<span class="selection__pricer">3 500 000 <span class="rub">P</span></span>
												<span class="selection__pricer">15 000 000 <span class="rub">P</span></span>
											</div>
										</div>
										<div class="selection__step step-2">
											<div class="selection__slider">
												<div class="selection__select-price-item">
													<span class="selection__select-price" id="range-2-span">1</span>
													<span class="toggle-ingo">м<sup>2</sup></span>
												</div>
												<div id="range-2"></div>
												<input type="hidden" name="range_2">
											</div>
											<div class="selection__price-container">
												<span class="selection__pricer">100 м<sup>2</sup></span>
												<span class="selection__pricer">300 м<sup>2</sup></span>
											</div>
										</div>
										<div class="selection__step step-3">
											<div class="selection__slider">
												<div class="selection__select-price-item">
													<span class="selection__select-price" id="range-3-span">1</span>
													<span class="toggle-ingo">соток</span>
												</div>
												<div id="range-3"></div>
												<input type="hidden" name="range_3">
											</div>
											<div class="selection__price-container">
												<span class="selection__pricer">7 соток</span>
												<span class="selection__pricer">20 соток</span>
											</div>
										</div>

										<div class="selection__panel">
											<div class="btn-main jsSelectionNext"><span>следующий вопрос</span></div>
											<div class="selection__prev-btn jsSelectionPrev"><span>предыдущий вопрос</span></div>
										</div>
									</div>

									<div class="selection__inner final">
										<div class="main-form__title-content">
											<h4 class="main-form__title"><span>Мы получили</span> ваши ответы</h4>
											<p class="main-form__subtitle">В течение 3 минут мы будем готовы отправить вам подборку домов</p>
										</div>
										<div class="main-form__field">
											<input type="tel" name="tel" placeholder="Введите ваш телефон">
										</div>
										<input type="hidden" name="form_name" value="Подбор дома">
                                        <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
										<button type="submit" class="btn-main"><span>Получить подборку <i>и узнать актуальные цены</i></span></button>
										<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
									</div>
								</form>
							</div>
						</div>
						<div class="selection__description-list">
							<div class="selection__description-item">
								<strong>12</strong>
								<p>достроенных коттеджей</p>
							</div>
							<div class="selection__description-item">
								<strong>24</strong>
								<p>объекта в процессе</p>
							</div>
						</div>
					</div>
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.02">
							<img src="img/bush-4.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.2">
							<img src="img/berry-p-2.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/tree-item.png" alt="parallax">
						</div>
					</div>
				</div>
			</section>

			<section class="projects">
				<div class="container">
					<div class="main-title">
						<h2>12 проектов домов площадью<br>от 135 м² <span>свободны для бронирования</span></h2>
					</div>
					<div class="projects__content">
						<div class="projects__image">
							<img src="img/house-big-1.jpg" alt="house">
						</div>
						<ul class="list-big  list-green">
							<li><strong>Частные</strong> дома и дома <span class="list-green__info-item">дуплекс</span></li>
							<li><strong>2</strong> этажа</li>
							<li><strong>Продуманные</strong> планировки</li>
							<li>В каждом доме<br><strong>балкон и мансарда</strong></li>
							<li><strong>Большие</strong> кухни-гостиные</li>
						</ul>
						<div class="projects__duplex-content">
							<div class="projects__duplex-wrap">
								<img src="img/duplex.svg" alt="duplex">
								<p>Дома дуплекс предназначены для проживания 2 семей</p>
							</div>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/tree-1.png" alt="parallax">
					</div>
				</div>
			</section>

			<section class="infrastructure">
				<div class="container">
					<div class="infrastructure__title-content">
						<div class="main-title">
							<h2>Инфраструктура<br>поселка позволяет проживать в загородном доме <span>круглый год</span></h2>
							<p>Или приезжать на выходные и с комфортом проводить время</p>
						</div>
						<div class="infrastructure__info">
							<img src="img/info-icon.png" alt="info">
							<p>Первая очередь уже заселена.</p>
							<p>Некоторые<br>живут в поселке постоянно</p>
						</div>
					</div>
					<div class="infrastructure__list">
						<div class="infrastructure__item">
							<div class="infrastructure__image">
								<img src="img/infrastructure-1.png" alt="infrastructure">
							</div>
							<strong>Центральные коммуникации</strong>
							<p>уже подведены: вода, газ, свет. канализация</p>
						</div>
						<div class="infrastructure__item">
							<div class="infrastructure__image">
								<img src="img/infrastructure-2.png" alt="infrastructure">
							</div>
							<strong>Подъездные дороги</strong>
							<p>к поселку уже построены — до Можайска 15 км, до Москвы 100 км</p>
						</div>
						<div class="infrastructure__item">
							<div class="infrastructure__image">
								<img src="img/infrastructure-3.png" alt="infrastructure">
							</div>
							<strong>Закрытая охраняемая территория</strong>
							<p>Въезд в поселок через пропускной пункт</p>
						</div>
						<div class="infrastructure__item">
							<div class="infrastructure__image">
								<img src="img/infrastructure-4.png" alt="infrastructure">
							</div>
							<strong>Детские площадки</strong>
							<p>Где ваши дети будут <br>с удовольствием играть</p>
						</div>
					</div>
					<div class="infrastructure__info  infrastructure__info--mobile">
						<img src="img/info-icon.png" alt="info">
						<div class="infrastructure__info-text">
							<p>Первая очередь уже заселена.</p>
							<p>Некоторые живут в поселке постоянно</p>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/tree-2.png" alt="parallax">
					</div>
				</div>
			</section>

			<section class="excursion" style="background-image: url(img/excursion-bg.jpg);">
				<div class="container">
					<div class="excursion__content">
						<div class="main-title">
							<h2>Приезжайте<br>в «Золотые пески» <span>на экскурсию</span></h2>
							<p>Прогуляйтесь по территории, посетите выбранные дома, познакомьтесь с соседями</p>
						</div>
						<strong class="list-title">Во время экскурсии вы:</strong>
						<ul class="list-big  list-green">
							<li>Сможете изучить район<br>и окрестности</li>
							<li>Своими глазами увидите,<br>как идут работы на площадке</li>
							<li>Зададите все интересующие вас вопросы</li>
							<li>Побываете в своем будущем доме и на участке</li>
						</ul>
					</div>
					<div class="main-form  main-form--space">
						<div class="main-form__wrap">
							<div class="main-form__title-content">
								<h4 class="main-form__title">Чтобы <span>записаться на экскурсию,</span></h4>
								<p class="main-form__subtitle">выберете день и оставьте свой<br>телефон для связи</p>
							</div>
							<form action="" class="tel-form" novalidate="novalidate">
								<div class="main-form__field">
									<input type="tel" name="tel" placeholder="Введите ваш телефон">
								</div>
								<button class="btn-main"><span>Скачать презентацию</span></button>
								<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
								<input type="hidden" name="form_name" value="Записаться на экскурсию">
                                <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
							</form>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/tree-3.png" alt="parallax">
					</div>
				</div>
			</section>

			<section class="opportunities" style="background-image: url(img/opportunities-bg.jpg);">
				<div class="container">
					<div class="main-title">
						<h2>Участки площадью <span>от 7 до 20 соток</span> с возможностью объединения</h2>
						<p>Вы сможете благоустроить участок по своему усмотрению</p>
					</div>
					<div class="opportunities__list">
						<div class="opportunities__item">
							<div class="opportunities__image">
								<img src="img/opportunities-1.png" alt="opportunities">
							</div>
							<span class="opportunities__num">01</span>
							<strong>Разбить огород или сад</strong>
							<p>и радовать свою семью свежими овощами и фруктами</p>
						</div>
						<div class="opportunities__item">
							<div class="opportunities__image">
								<img src="img/opportunities-2.png" alt="opportunities">
							</div>
							<span class="opportunities__num">02</span>
							<strong>Создать уникальный ландшафтный дизайн<br>в своем дворе</strong>
							<p>и наслаждаться прекрасным видом из окна</p>
						</div>
						<div class="opportunities__item">
							<div class="opportunities__image">
								<img src="img/opportunities-3.png" alt="opportunities">
							</div>
							<span class="opportunities__num">03</span>
							<strong>Построить детскую или спортивную площадку</strong>
							<p>и весело проводить время<br>с близкими и друзьями</p>
						</div>
						<div class="opportunities__item">
							<div class="opportunities__image">
								<img src="img/opportunities-4.png" alt="opportunities">
							</div>
							<span class="opportunities__num">04</span>
							<strong>Обустроить зону барбекю</strong>
							<p>и готовить на огне<br>любимые блюда</p>
						</div>
						<div class="opportunities__item">
							<div class="opportunities__image">
								<img src="img/opportunities-5.png" alt="opportunities">
							</div>
							<span class="opportunities__num">05</span>
							<strong>Возвести баню или сауну</strong>
							<p>и устраивать настоящие<br>спа-процедуры у себя дома</p>
						</div>
					</div>
				</div>
			</section>

			<section class="materials">
				<div class="container">
					<div class="main-title">
						<h2>Поселок выполнен <span>в едином архитектурном стиле</span></h2>
						<p>При строительстве использованы безопасные материалы</p>
					</div>
					<div class="materials__list">
						<div class="materials__item">
							<div class="materials__image">
								<img src="img/materials-1.png" alt="materials">
							</div>
							<strong>Газосиликатный блок</strong>
							<p>Негорючий материал, эффективно сохраняет тепло, превосходная изоляция уличного шума</p>
						</div>
						<div class="materials__item">
							<div class="materials__image">
								<img src="img/materials-2.png" alt="materials">
							</div>
							<strong>Ленточный фундамент</strong>
							<p>Выдерживает большие<br>нагрузки</p>
						</div>
						<div class="materials__item">
							<div class="materials__image">
								<img src="img/materials-3.png" alt="materials">
							</div>
							<strong>Оцилинрованное бревно</strong>
							<p>Хорошая теплоизоляция, эффективная звукоизоляция, поддерживает комфортную температуру в доме, экологически безопасно, долговечно</p>
						</div>
					</div>
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.2">
							<img src="img/leaf-p-4.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.1">
							<img src="img/leaf-p-5.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.4">
							<img src="img/leaf-p-6.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/leaf-p-7.png" alt="parallax">
						</div>
					</div>
				</div>
			</section>

			<section class="plan">
				<div class="container">
					<div class="plan__content">
						<div class="main-title">
							<h2><span>Скачайте</span> генеральный план поселка</h2>
						</div>
						<div class="main-form">
							<div class="main-form__wrap">
								<div class="main-form__title-content">
									<h4 class="main-form__title">Введите<br><span>ваши данные,</span></h4>
									<p class="main-form__subtitle">чтобы скачать презентацию в 1 клик</p>
								</div>
								<form action="" class="name-tel-form" novalidate="novalidate">
									<div class="main-form__field">
										<input type="text" name="name" placeholder="Введите ваше имя">
									</div>
									<div class="main-form__field">
										<input type="tel" name="tel" placeholder="Введите ваш телефон">
									</div>
									<span class="main-form__small-text">Презентация откроется автоматически в новой вкладке</span>
									<button class="btn-main"><span>Скачать презентацию</span></button>
									<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
									<input type="hidden" name="form_name" value="Скачать полную презентацию">
                                    <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
								</form>
							</div>
						</div>
					</div>
					<div class="plan__list">
						<div class="plan__item">
							<img src="img/checked-g.png" alt="check">
							<p>Расположение <br>участков</p>
						</div>
						<div class="plan__item">
							<img src="img/checked-g.png" alt="check">
							<p>Участки <br>с возможностью <br>объединения</p>
						</div>
						<div class="plan__item">
							<img src="img/checked-g.png" alt="check">
							<p>Инфраструктурные <br>объекты</p>
						</div>
						<div class="plan__item">
							<img src="img/checked-g.png" alt="check">
							<p>Дороги внутри <br>поселка</p>
						</div>
					</div>
					<img src="img/book-1.jpg" alt="book" class="plan__book">
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.1">
							<img src="img/pen.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.3">
							<img src="img/eraser.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/leaf-p-2.png" alt="parallax">
						</div>
					</div>
					<div class="parallax-wrap  parallax-wrap--index">
						<div class="parallax-item  layer" data-depth="0.1">
							<img src="img/berry-p-2.png" alt="parallax">
						</div>
					</div>
				</div>
			</section>

			<section class="building">
				<div class="container">
					<div class="main-title">
						<h2><span>Надежный</span> застройщик AR Group</h2>
						<p>Специализируется на возведении жилых объектов в элитном сегменте</p>
					</div>
					<strong class="building__strong">Объекты AR Group строятся в срок или с опережением сроков</strong>
					<div class="building__list">
						<div class="building__item">
							<div class="building__wrap">
								<div class="building__text-content">
									<div class="building__image">
										<img src="img/build-1.png" alt="house">
									</div>
									<strong>ЖК «Белый дворец»</strong>
									<span class="building__city">Сочи</span>
									<p>Жилой высотный комплекс премиум-класса на 750 квартир и 2-уровневой подземной парковкой</p>
								</div>
								<a href="http://www.white-palace.ru/" target="_blank" class="building__link">www.white-palace.ru</a>
							</div>
						</div>
						<div class="building__item">
							<div class="building__wrap">
								<div class="building__text-content">
									<div class="building__image">
										<img src="img/build-2.png" alt="house">
									</div>
									<strong>ЖК «Морская симфония»</strong>
									<span class="building__city">Сочи</span>
									<p>Жилой комплекс повышенной комфортности на 240 квартир</p>
								</div>
								<a href="http://www.morsim.ru/" target="_blank" class="building__link">www.morsim.ru</a>
							</div>
						</div>
						<div class="building__item">
							<div class="building__wrap">
								<div class="building__text-content">
									<div class="building__image">
										<img src="img/build-3.png" alt="house">
									</div>
									<strong>АК «Лучезарный»</strong>
									<span class="building__city">Сочи</span>
									<p>Элитный 7-этажный апарт-комплекс на берегу Черного моря с бассейном и собственным парком 3,2 Га</p>
								</div>
								<a href="https://luchezarniy.ru/" target="_blank" class="building__link">luchezarniy.ru</a>
							</div>
						</div>
					</div>
					<div class="building__docs-content">
						<div class="main-form  main-form--small">
							<div class="main-form__wrap">
								<div class="main-form__title-content">
									<h4 class="main-form__title"><span>Получите весь <br>пакет документов</span> для скачивания</h4>
									<p class="main-form__subtitle">Включая договор купли-продажи <br>и проектную декларацию</p>
								</div>
								<form action="" class="mail-tel-form" novalidate="novalidate">
									<div class="main-form__field">
										<input type="email" name="email" placeholder="Введите вашу почту">
									</div>
									<div class="main-form__field">
										<input type="tel" name="tel" placeholder="Введите ваш телефон">
									</div>
									<button class="btn-main"><span>скачать пакет документов</span></button>
									<input type="hidden" name="form_name" value="Получить весь пакет документов">
                                    <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
								</form>
							</div>
						</div>
						<div class="building__docs-list">
							<div class="building__docs-item">
								<div class="building__docs-image">
									<img src="img/doc-1.jpg" alt="docs">
								</div>
								<strong>Выписка из ЕГРН</strong>
								<p>Из документа вы узнаете сведения об объекте недвижимости, зарегистрированные права на объект и убедитесь, что объект юридически чист</p>
							</div>
							<div class="building__docs-item">
								<div class="building__docs-image">
									<img src="img/doc-2.jpg" alt="docs">
								</div>
								<strong>Разрешение <br>на строительство</strong>
								<p>Убедитесь, что застройщик имеет законное право осуществлять строительство</p>
							</div>
						</div>
					</div>
				</div>
				<div class="parallax-wrap">
					<div class="parallax-item  layer" data-depth="0.15">
						<img src="img/leaf-p-8.png" alt="parallax">
					</div>
					<div class="parallax-item  layer" data-depth="0.3">
						<img src="img/berry-p-3.png" alt="parallax">
					</div>
					<div class="parallax-item  layer" data-depth="0.05">
						<img src="img/berry-p-4.png" alt="parallax">
					</div>
					<div class="parallax-item  layer" data-depth="0.02">
						<img src="img/bush-2.png" alt="parallax">
					</div>
				</div>
			</section>

			<section class="payment">
				<div class="container">
					<div class="payment__content">
						<div class="main-title">
							<h2>Возможна <span>ипотека от Сбербанка <br>или рассрочка</span> <br>на 12 месяцев</h2>
						</div>
						<div class="payment__content-info">
							<ul class="list-big  list-green">
								<li>Высокая вероятность одобрения</li>
								<li>Наши специалисты помогут подготовить документы <br>и сэкономят ваше время</li>
								<li>Скорость одобрения <br>от 3 часов</li>
							</ul>
							<div class="payment__price-info">
								<strong>Рассрочка</strong>
								<p>Удобный график и выгодное распределение платежей</p>
							</div>
						</div>
					</div>
					<div class="main-form">
						<div class="main-form__wrap">
							<div class="main-form__title-content">
								<h4 class="main-form__title"><span>Оставьте заявку</span> <br>и узнайте,</h4>
								<p class="main-form__subtitle">какой способ оплаты будет для вас наиболее выгодным</p>
							</div>
							<form form action="" class="tel-form" novalidate="novalidate">
								<div class="main-form__field">
									<input type="tel" name="tel" placeholder="Введите ваш телефон">
								</div>
								<span class="main-form__small-text">Менеджер отдела продаж рассчитает <br>первый платеж  и составит график выплат <br>по ипотеке или рассрочке</span>
								<button class="btn-main"><span>оставить заявку</span></button>
								<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
								<input type="hidden" name="form_name" value="Заявка: выгодный способ оплаты">
                                <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
							</form>
						</div>
					</div>
					<img src="img/bank-doc.png" alt="bank-doc" class="payment__bank-doc">
					<div class="parallax-wrap">
						<div class="parallax-item  layer" data-depth="0.2">
							<img src="img/money-1.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.1">
							<img src="img/money-2.png" alt="parallax">
						</div>
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/marker.png" alt="parallax">
						</div>
					</div>
					<div class="parallax-wrap  parallax-wrap--index">
						<div class="parallax-item  layer" data-depth="0.05">
							<img src="img/leaf-p-2.png" alt="parallax">
						</div>
					</div>
				</div>
			</section>

			<section class="map-block" style="background-image: url(img/map-block-bg.jpg);">
				<div class="container">
					<div class="map-block__info  map-block__info--mobile">
						<h4>Остались <span>вопросы?</span></h4>
						<p class="map-block__description">Приезжайте в офис отдела продаж</p>
						<div class="map-block__contacts">
							<div class="map-block__contacts-item">
								<strong>адрес</strong>
								<p>МО, Можайский район, <br>пос. Золотые пески</p>
							</div>
							<div class="map-block__contacts-item">
								<strong>Режим работы</strong>
								<p>Пн-пт 10.00-18.00</p>
								<p>Сб 10.00-15.00</p>
							</div>
							<div class="map-block__contacts-item">
								<strong>телефон</strong>
								<a href="tel:89999992511" class="map-block__tel jsTrackContact">89999992511</a>
							</div>
							<div class="map-block__contacts-item">
								<strong>почта</strong>
								<a href="mailto:sale@zpnew.ru" class="map-block__email jsTrackContact">sale@zpnew.ru</a>
							</div>
						</div>
					</div>
					<div class="map-block__content">
						<div id="map"></div>
						<div class="map-block__info">
							<h4>Остались <span>вопросы?</span></h4>
							<p class="map-block__description">Приезжайте в офис отдела продаж</p>
							<div class="map-block__contacts">
								<div class="map-block__contacts-item">
									<strong>адрес</strong>
									<p>МО, Можайский район, <br>пос. Золотые пески</p>
								</div>
								<div class="map-block__contacts-item">
									<strong>Режим работы</strong>
									<p>Пн-пт 10.00-18.00</p>
									<p>Сб 10.00-15.00</p>
								</div>
								<div class="map-block__contacts-item">
									<strong>телефон</strong>
									<a href="tel:89999992511" class="map-block__tel jsTrackContact">8 999 999 25 11</a>
								</div>
								<div class="map-block__contacts-item">
									<strong>почта</strong>
									<a href="mailto:sale@zpnew.ru" class="map-block__email jsTrackContact">sale@zpnew.ru</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<footer class="footer">
				<div class="container">
					<div class="footer__logo-content">
						<a href="/" class="footer__logo"><img src="img/logo-footer.svg" alt="logo"></a>
						<span>Коттеджный поселок бизнес-класса</span>
					</div>
					<div class="footer__info">
						<ul class="footer__links">
							<li><a href="#">Проектные декларации</a></li>
							<li><a href="#">Разрешительная документация</a></li>
							<li><a href="#">Политика конфиденциальности</a></li>
						</ul>
						<div class="footer__social-content">
							<!--<a href="#" target="_blank" class="footer__social">-->
								<!--<img width="18" src="img/vk.svg" alt="vk">-->
							<!--</a>-->
							<a href="https://www.facebook.com/zolotpeski/" target="_blank" class="footer__social">
								<img width="18" src="img/facebook.svg" alt="facebook">
							</a>
							<a href="https://www.instagram.com/zpeski" target="_blank" class="footer__social">
								<img width="15" src="img/instagram.svg" alt="instagram">
							</a>
						</div>
						<div class="footer__tel-content">
							<a href="tel:89999992511" class="footer__tel jsTrackContact">8 999 999 25 11</a>
							<a href="mailto:sale@zpnew.ru" class="footer__mail jsTrackContact">sale@zpnew.ru</a>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<div class="popup  main-form  main-form--space" id="popup-1">
		<div class="main-form__wrap">
			<div class="main-form__title-content">
				<h4 class="main-form__title">Введите ваш <br><span>номер телефона,</span></span></h4>
				<p class="main-form__subtitle">Специалист отдела продаж позвонит вам сейчас или в удобное для звонка время</p>
			</div>
			<form action="" class="tel-form" novalidate="novalidate">
				<div class="main-form__field">
					<input type="tel" name="tel" placeholder="Введите ваш телефон">
				</div>
				<button class="btn-main"><span>позвоните мне</span></button>
				<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
				<input type="hidden" name="form_name" value="Перезвонить Клиенту">
				<input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
			</form>
			<img class="popup__leaf" src="img/leaf-p-2.png" alt="leaf">
			<img class="popup__berry" src="img/berry-p-2.png" alt="berry">
		</div>
	</div>
	<div class="popup-big  main-form  main-form--space" id="popup-2">
		<div class="main-form__wrap">
			<div class="popup-big__bg" style="background-image: url(img/popup-bg-1.jpg);"></div>
			<div class="popup-big__main">
				<div class="main-form__title-content">
					<h4 class="main-form__title">Подобрать дом <br><span>под свой бюджет</span></h4>
					<p class="main-form__subtitle">специалист отдела продаж свяжется <br>с вами по указанному номеру</p>
				</div>
				<form action="" class="tel-form" novalidate="novalidate">
					<div class="main-form__field">
						<input type="tel" name="tel" placeholder="Введите ваш телефон">
					</div>
					<button class="btn-main"><span>подобрать дом</span></button>
					<p class="main-form__description">Нажимая на кнопку, вы принимаете<br>условия <a href="#">передачи данных</a></p>
					<input type="hidden" name="form_name" value="Подобрать дом">
                    <input type="hidden" name="utm" value="<?php echo htmlspecialchars(json_encode($_GET)); ?>">
				</form>
			</div>
			<img class="popup__leaf" src="img/leaf-p-2.png" alt="leaf">
			<img class="popup__berry" src="img/berry-p-2.png" alt="berry">
		</div>
	</div>

	<video width="1280" height="720" controls id="myVideo" style="display:none;">
	    <source src="video.mp4" type="video/mp4">
	    Your browser doesn't support HTML5 video tag.
	</video>

	<script src="js/vendor.js"></script>
	<script src="js/main.js"></script>
	<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhjCQNBCef_6oHiYOGRdVRAJRz8fNfT6Q&amp;callback=initMap"></script>
</body>
</html>