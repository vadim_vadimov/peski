var map;
function initMap() {
  var mapCenterCoords;
  if ($(window).width() > 723) {
      mapCenterCoords = {lat: 55.625376, lng: 35.768077};
  } else {
      mapCenterCoords = {lat: 55.625376, lng: 35.771077};
  }
 map = new google.maps.Map(document.getElementById('map'), {
  center: mapCenterCoords,
   zoom: 17,
   scrollwheel: false,
   zoomControl: true,
   disableDefaultUI: true, 
   styles: [
     
     {elementType: 'labels.text.stroke', stylers: [{color: '#ffffff'}]},
     {elementType: 'labels.text.fill', stylers: [{color: '#84898b'}]},
     {
       featureType: 'administrative.locality',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'poi',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'poi.park',
       elementType: 'labels.text.stroke',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'road',
       elementType: 'geometry',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'road',
       elementType: 'geometry.stroke',
       stylers: [{color: '#e5e5e5'}]
     },
     {
       featureType: 'road',
       elementType: 'labels.text.fill',
       stylers: [{color: '#84898b'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'geometry',
       stylers: [{color: '#d8d8d8'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'geometry.stroke',
       stylers: [{color: '#acacac'}]
     },
     {
       featureType: 'road.highway',
       elementType: 'labels.text.fill',
       stylers: [{color: '#414141'}]
     },
     {
       featureType: 'transit',
       elementType: 'geometry',
       stylers: [{color: '#ffffff'}]
     },
     {
       featureType: 'transit.station',
       elementType: 'labels.text.fill',
       stylers: [{color: '#ffffff'}]
     }
   ]
 });

 var marker;
  marker = new google.maps.Marker({

   position: {lat: 55.625376, lng: 35.771077},          
   map: map,                           
   icon:"img/map-marker.png"
 });


};

function selection() {
    var $inputRange1 = $('input[name="range_1"]'),
        $inputRange2 = $('input[name="range_2"]'),
        $inputRange3 = $('input[name="range_3"]');

    var fbPixelQuizFlag = true;

    if($('#range-1').length > 0) {
        var slider = document.getElementById('range-1');
        var slider1Value = document.getElementById('range-1-span');

        noUiSlider.create(slider, {
            start: [0],
            connect: [true, false],
            step: 500000,
            range: {
                'min': 3500000,
                'max': 15000000
            },
            ariaFormat: wNumb({
                decimals: 3
            }),
            format: wNumb({
                decimals: 0,
                thousand: ' ',
            }),
            pips: {
                    mode: 'values',
                    values: [3500000, 15000000],
                    density: 4.3
            }
        });

        slider.noUiSlider.on('update', function (values, handle) {
            slider1Value.innerHTML = values[handle];
            $inputRange1.val(values[handle]);
        });
    }
    if($('#range-2').length > 0) {
        var slider2 = document.getElementById('range-2');
        var slider2Value = document.getElementById('range-2-span');

        noUiSlider.create(slider2, {
            start: [0],
            connect: [true, false],
            step: 100,
            range: {
                'min': 100,
                'max': 300
            },
            format: {
                to: function (value) {
                    return value + '';
                },
                from: function (value) {
                    return value.replace(' ', '');
                }
            },
            pips: {
                    mode: 'values',
                    values: [100, 300],
                    density: 60
            }
        });

        slider2.noUiSlider.on('update', function (values, handle) {
            slider2Value.innerHTML = values[handle];
            $inputRange2.val(values[handle]);
        });
    }
    if($('#range-3').length > 0) {
        var slider3 = document.getElementById('range-3');
        var slider3Value = document.getElementById('range-3-span');

        noUiSlider.create(slider3, {
            start: [7],
            connect: [true, false],
            step: 5,
            snap: true,
            range: {
                'min': 7,
                '33.333%': 10,
                '66.9%': 15,
                'max': 20,
            },
            format: {
                to: function (value) {
                    return value + '';
                },
                from: function (value) {
                    return value.replace(' ', '');
                }
            },
            pips: {
                    mode: 'values',
                    values: [7, 20],
                    density: 40
            }
        });

        slider3.noUiSlider.on('update', function (values, handle) {
            slider3Value.innerHTML = values[handle];
            $inputRange3.val(values[handle]);
        });
    }

    var currentStep = 1,
        fadeSpeed = 500,
        $btnNext = $('.jsSelectionNext'),
        $btnPrev = $('.jsSelectionPrev'),
        $title1 = $('.selection__item-title.step-1'),
        $title2 = $('.selection__item-title.step-2'),
        $title3 = $('.selection__item-title.step-3'),
        $step1 = $('.selection__step.step-1'),
        $step2 = $('.selection__step.step-2'),
        $step3 = $('.selection__step.step-3'),
        $bar = $('.selection__questions'),
        $currStepNum = $('.jsSelectionStep');
    $btnNext.on('click', function (e) {
        e.preventDefault();
        if (currentStep === 1) {
            $title1.hide();
            $title2.fadeIn(fadeSpeed);
            $step1.hide();
            $step2.fadeIn(fadeSpeed);

            $bar.removeClass('step-1').addClass('step-2');
            $btnPrev.fadeIn(fadeSpeed);
            currentStep = 2;
        } else if (currentStep === 2) {
            $title2.hide();
            $title3.fadeIn(fadeSpeed);
            $step2.hide();
            $step3.fadeIn(fadeSpeed);

            $bar.removeClass('step-2').addClass('step-3');
            currentStep = 3;
            $btnNext.html('<span>завершить подбор</span>');
        } else if (currentStep === 3) {
            // show final!
            $('.selection__inner').hide();
            $('.selection__inner.final').fadeIn(fadeSpeed);
        }

        setTimeout(function () {
            $currStepNum.html(currentStep);
        }, 400);

        if (fbPixelQuizFlag) {
            fbq('track', 'Quiz_zp', {
                value: 3,
            });
            fbPixelQuizFlag = false;
        }
    });
    $btnPrev.on('click', function (e) {
        e.preventDefault();
        if (currentStep === 1) {
            return false;
        } else if (currentStep === 2) {
            $title2.hide();
            $title1.fadeIn(fadeSpeed);
            $step2.hide();
            $step1.fadeIn(fadeSpeed);

            $bar.removeClass('step-2').addClass('step-1');
            $btnPrev.hide();

            currentStep = 1;
        } else if (currentStep === 3) {
            $title3.hide();
            $title2.fadeIn(fadeSpeed);
            $step3.hide();
            $step2.fadeIn(fadeSpeed);

            $bar.removeClass('step-3').addClass('step-2');

            currentStep = 2;
            $btnNext.html('<span>следующий вопрос</span>');
        }

        setTimeout(function () {
            $currStepNum.html(currentStep);
        }, 400);
    });

}
selection();







$(function () {


  $.extend( $.validator.messages, {
    required: "Это поле необходимо заполнить.",
    remote: "Пожалуйста, введите правильное значение.",
    email: "Пожалуйста, введите корректный адрес электронной почты.",
    url: "Пожалуйста, введите корректный URL.",
    date: "Пожалуйста, введите корректную дату.",
    dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
    number: "Пожалуйста, введите число.",
    digits: "Пожалуйста, вводите только цифры.",
    creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
    equalTo: "Пожалуйста, введите такое же значение ещё раз.",
    extension: "Пожалуйста, выберите файл с правильным расширением.",
    maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
    minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
    rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
    range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
    max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
    min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
  } );






	if ($(window).width() > 1023) {

	  $('.parallax-wrap').parallax({
	    calibrateX: true,
	    calibrateY: false,
	    limitX: false,
	    limitY: false,
	    scalarX: 2,
	    scalarY: 10,
	    frictionX: 0.2,
	    frictionY: 0.2
	  });

	}


  $('.tel-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 required: true,
             },

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "tel-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 window.location = "thanks-page.html";
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });


    $('.selection-form').each(function(i, item) {
        var $form = $(item);

        $form.validate({
            rules: {
                tel: {
                    required: true,
                },
            },
            messages: {
                tel: {
                    required: 'Укажите ваш телефон'
                },
            },
            submitHandler: function(form) {
                var $form = $(form);
                $.ajax({
                    type: "POST",
                    url: "selection-field.php",
                    data: $form.serialize()
                }).done(function(e) {
                    $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                    $form.trigger('reset');

                    window.location = "thanks-page.html";

                }).fail(function (e) {
                    $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                    $form.trigger('reset');
                    setTimeout(function () {
                        $form.find('.dispatch-message').slideUp();
                    }, 5000);
                })
            }
        });
    });


    $('.name-tel-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 required: true,
             },
             name: {
                 required: true
             }

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
             name: {
                 required: 'Укажите ваше имя'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "name-tel-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                window.location = "thanks-page.html";
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('.date-tel-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 required: true,
             },
             date: {
                 required: true
             }

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
             date: {
                 required: 'Укажите дату'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "date-tel-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 window.location = "thanks-page.html";
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('.mail-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             email: {
                 required: true
             }

         },
         messages: {
             email: {
                 required: 'Укажите ваш e-mail'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "mail-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 window.location = "thanks-page.html";
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('.mail-tel-form').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
            tel: {
                 required: true,
             },
             email: {
                 required: true
             }

         },
         messages: {
            tel: {
                 required: 'Укажите ваш телефон'
             },
             email: {
                 required: 'Укажите ваш e-mail'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "mail-tel-field.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');
                 window.location = "thanks-page.html";
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });

  $('input[name="tel"]').inputmask({mask: "+7 (999) 9999999"});


  $( ".list-green__info-item" ).hover (function() {
      $('.projects__duplex-content').toggleClass('active');
  });





    $("[data-fancybox]").fancybox({
        afterShow: function( instance, slide ) {
            if (slide.contentType === 'html') {
                fbq('track', 'FormOpen_zp', {
                    value: 2
                });
            }
            if (slide.contentType === 'video') {
                fbq('track', 'Video_zp', {
                    value: 2
                });
            }

        }
    });
    $('.jsTrackContact').on('click', function () {
        fbq('track', 'Contact_zp', {
            value: 4
        });
    });



});